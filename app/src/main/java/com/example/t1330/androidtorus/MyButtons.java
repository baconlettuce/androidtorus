package com.example.t1330.androidtorus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.Random;

/**
 * Created by t1330 on 2016/03/16.
 */
public class MyButtons {
    protected MyButtonRC[] mybuttonrow;
    protected MyButtonRC[] mybuttoncol;
    
    private Context context;
    private int row_size;
    private int col_size;
    private int buttonsize;

    private MyButtons rmybuttons;

    MyButtons(Context context, int row_size, int col_size, int buttonsize) {
        setContext(context);
        setsize(row_size, col_size);
        setbuttonsize(buttonsize);
        
        mybuttonrow = new MyButtonRC[row_size];
        mybuttoncol = new MyButtonRC[col_size];

        // MyButtonの生成
        MyButton[][] mybuttons = new MyButton[row_size][col_size];
        for (int j = 0; j < row_size; j++) {
            for (int i = 0; i < col_size; i++) {
//                mybuttons[j][i] = new MyButton(context, String.valueOf(j * col_size + i + 1));
                mybuttons[j][i] = new MyButton(context, String.valueOf(j * col_size + i + 1));
            }
        }

        // MyButtonのmybuttonrowへの割り当て
        for (int j=0; j < row_size; j++) {
            mybuttonrow[j] = new MyButtonRC(mybuttons[j]);
        }
        // MyButtonのmybuttoncolへの割り当て
        for (int i=0; i<col_size; i++) {
            mybuttoncol[i] = new MyButtonRC(row_size);
            for (int j=0; j < row_size; j++) {
                mybuttoncol[i].setMyButton(mybuttons[j][i], j);
            }
        }
        makeBitmap();
        rmybuttons = this.reverse();
    }
    MyButtons(Context context) {
        this.context = context;
    }
    void setContext(Context context) {
        this.context = context;
    }
    void setsize(int row_size, int col_size) {
        this.row_size = row_size;
        this.col_size = col_size;
    }
    void setbuttonsize(int buttonsize) {
        this.buttonsize = buttonsize;
    }
    boolean judge() {
        for (int i=0; i<row_size; i++) {
            for (int j=0; j<col_size; j++) {
                if (!getMyButton(i,j).judge()) {
                    return false;
                }
            }
        }
       return true;
    }
    void switchisdisplayimage() {
        MyButton.setIsImageDisplay(!MyButton.getIsImageDisplay()); 
        for (int i=0; i<col_size; i++) {
            for (int j=0; j<row_size; j++) {
                getMyButton(i,j).invalidate();
            }
        }
    }
    void shuffle() {
        Random rnd = new Random();
        int num = rnd.nextInt(row_size+col_size);
        for( int i=0; i<num; i++) {
            int row = rnd.nextInt(row_size-1)+1;
            int col = rnd.nextInt(col_size-1)+1;
            int r_num = rnd.nextInt(col_size-1)+1;
            int c_num = rnd.nextInt(row_size-1)+1;
            for (int j=0; j<r_num; j++) {
                rowswap(row);
            }
            for (int j=0; j<c_num; j++) {
                colswap(col);
            }
        }
        if(judge()) {
            shuffle();
        }
    }
    int getrow_size() {
        return row_size;
    }
    int getcol_size() {
        return col_size;
    }
    int getbuttonsize() {
        return buttonsize;
    }
    MyButtons getrmybuttons() {
        return rmybuttons;
    }

    MyButtons reverse() {
        MyButtons rbuttons = new MyButtons(this.context);
        rbuttons.setsize(this.col_size, this.row_size);
        rbuttons.mybuttonrow = this.mybuttoncol;
        rbuttons.mybuttoncol = this.mybuttonrow;

        rbuttons.rmybuttons = this;
        return rbuttons;
    }
    private void makeBitmap() {
        Bitmap droidBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.droid);
        int imageHeight = droidBitmap.getHeight();
        int imageWidth = droidBitmap.getWidth();
        if ( imageHeight > imageWidth) {
            droidBitmap = Bitmap. createBitmap(droidBitmap, 0, 0, imageWidth, imageWidth);
        } else if (imageHeight< imageWidth) {
            droidBitmap = Bitmap.createBitmap(droidBitmap, 0,0, imageHeight, imageHeight);
        }
        int bestsize = buttonsize;
        droidBitmap=Bitmap.createScaledBitmap(droidBitmap, bestsize*row_size, bestsize*col_size, false);

        for (int i=0; i<row_size; i++) {
            for (int j=0; j<col_size; j++) {
                MyButton mbi = getMyButton(j,i);
                int x= bestsize*i;
                int y= bestsize*j;
                mbi.setBitmap(Bitmap.createBitmap(droidBitmap, x, y, bestsize, bestsize));
            }
        }
    }
    void colswap(int col) {
        mybuttoncol[col].swap();
    }
    void colbackswap(int col) {
        mybuttoncol[col].backswap();
    }
    void rowswap(int row) {
        mybuttonrow[row].swap();
    }
    void rowbackswap(int row) {
        mybuttonrow[row].backswap();
    }
    MyButtonRC getMyButtonrow(int row) {
        return mybuttonrow[row];
    }
    MyButtonRC getMyButtoncol(int col) {
        return mybuttoncol[col];
    }
    MyButton getMyButton(int row, int col) {
        return mybuttonrow[row].getMyButton(col);
    }
    String getNum(int row, int col) {
        return getMyButton(row, col).getNum();
    }
    void setNum(String num, int row, int col) {
        getMyButton(row, col).setNum(num);
    }
    void setMyButton(MyButton abutton, int row, int col) {
        mybuttonrow[row].setMyButton(abutton,col);
    }

}
