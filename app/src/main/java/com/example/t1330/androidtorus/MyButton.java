package com.example.t1330.androidtorus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.Button;

/**
 * Created by t1330 on 2016/03/16.
 */
public class MyButton extends Button {
    Bdata bdata;
    Paint paint;
    protected String bid;
    protected static boolean isimagedisplay=false;

    MyButton(Context context) {
        super(context);
        bdata = new Bdata();
    }
    MyButton(Context context, String num) {
        super(context);
        bdata = new Bdata(num);
        bid=num;
        setText();
    }
    public void setText() {
        setText(bdata.getNum());
    }
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (getIsImageDisplay()){
            Bitmap bitmap = getBitmap();
            if (paint==null) {
                paint = new Paint();
            }
            if (bitmap!=null) {
                canvas.drawBitmap(bitmap, 0, 0, paint);
            }
        }
        else {
            setText();
        }
    }
    public void setNum(String num) {
        bdata.setNum(num);
        setText(num);
    }
    public void setNum(MyButton a) {
        bdata.setNum(a.getNum());
        setText(a.getNum());
    }
    public void  setBitmap(Bitmap bitmap) {
        bdata.setBitmap(bitmap);
    }
    public static  void setIsImageDisplay(boolean isdisplay) {
        isimagedisplay = isdisplay;
    }
    public static boolean getIsImageDisplay() {
        return isimagedisplay;
    }
    public void setBdata(Bdata bdata) {
        this.bdata = bdata;
        setBitmap(bdata.getBitmap());
        invalidate();
    }
    String getNum() {
        return bdata.getNum();
    }
    Bitmap getBitmap() {
        return bdata.getBitmap();
    }
    Bdata getBdata() {
        return this.bdata;
    }
    boolean judge() {
        return bid.equals(getNum());
    }
}
class Bdata {
    private String num;
    private Bitmap bitmap;
    Bdata() {
    }
    Bdata(String num) {
        setNum(num);
    }
    public void setNum(String num) {
        this.num = num;
    }
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    String getNum() {
        return this.num;
    }
    Bitmap getBitmap() {
        return this.bitmap;
    }
}
