package com.example.t1330.androidtorus;

/**
 * Created by t1330 on 2016/03/25.
 */
public class HistList {
    HistLink top;

    HistList() {
    }
    void push(RCButton button) {
        HistLink link = new HistLink(button);
        if (top==null) {
            top=link;
            return;
        }
        link.setNext(top);
        top.setLast(link);
        top = link;
    }
    HistLink pop() {
        if(top==null)
            return null;
        HistLink link = top;
        top = top.getNext();
        return link;
    }
}
class HistLink {
    private RCButton button;
    private HistLink next;
    private HistLink last;

    HistLink(RCButton button) {
        setRCButton(button);
    }
    void setRCButton(RCButton button) {
        this.button = button;
    }
    void setNext(HistLink next) {
        this.next = next;
    }
    void setLast(HistLink last) {
        this.last = last;
    }
    RCButton getButton() {
        return button;
    }
    HistLink getNext() {
        return next;
    }
    HistLink getLast() {
        return last;
    }
}
