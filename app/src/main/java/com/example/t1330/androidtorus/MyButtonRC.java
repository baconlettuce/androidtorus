package com.example.t1330.androidtorus;

import android.content.Context;

/**
 * Created by t1330 on 2016/03/27.
 */
public class MyButtonRC {
	private MyButton[] mybuttonrow;
	private int col_size;
	private RCButton rcbutton;

	MyButtonRC(MyButton[] mybuttonrow) {
		setMyButtonRow(mybuttonrow);
	}
	MyButtonRC(int col_size) {
		setcol_size(col_size);
		mybuttonrow = new MyButton[col_size];
	}
	void setMyButtonRow(MyButton[] mybuttonrow) {
		this.mybuttonrow = mybuttonrow;
		setcol_size(mybuttonrow.length);
	}
	Bdata getBdata(int col) {
		MyButton mybuttonimage = getMyButton(col);
		return mybuttonimage.getBdata();
	}
	void setBdata(Bdata bdata, int col) {
		MyButton mybuttonimage = getMyButton(col);
		mybuttonimage.setBdata(bdata);
	}
	void swap() {
		Bdata[] tmp = new Bdata[2];
		int j=0;
		tmp[j] = getBdata(col_size-1);
		j=(j+1)%2;
		for (int i=col_size-1; i>0; i--) {
			tmp[j] = getBdata(i-1);
			j=(j+1)%2;
			setBdata(tmp[j], i-1);
		}
		j=(j+1)%2;
		setBdata(tmp[j], col_size - 1);
	}
	void backswap() {
		Bdata[]tmp = new Bdata[2];
		int j=0;
		tmp[j] = getBdata(0);
		j=(j+1)%2;
		for(int i=1; i<col_size; i++) {
			tmp[j] = getBdata(i);
			j=(j+1)%2;
			setBdata(tmp[j],i);
		}
		j=(j+1)%2;
		setBdata(tmp[j], 0);
	}
	MyButton getMyButton(int col) {
		return mybuttonrow[col];
	}
	String getNum(int col) {
		return getMyButton(col).getNum();
	}
	RCButton makeRCButton(Context context) {
		rcbutton = new RCButton(context, this);
		return rcbutton;
	}
	void setNum(String num, int col) {
		getMyButton(col).setNum(num);
	}
	void setMyButton(MyButton abutton, int col) {
		mybuttonrow[col] = abutton;
	}
	private void setcol_size(int col_size) {
		this.col_size = col_size;
	}
	int getcol_size() {
		return col_size;
	}
}
