package com.example.t1330.androidtorus;

import android.content.Context;
import android.view.View;
import android.widget.Button;

/**
 * Created by t1330 on 2016/03/25.
 */
public class RCButton extends Button {
    MyButtonRC mybuttonrc;
    protected static HistList history;
    protected static HistList redolist;
    static View.OnClickListener listen;

    RCButton(Context context, MyButtonRC mybuttonrc) {
        super(context);
        this.mybuttonrc = mybuttonrc;
        setOnClickListener(listen);
    }
    static void init(View.OnClickListener listener, HistList list) {
        listen = listener;
        history = list;
    }
    static HistList getHistory() {
        return history;
    }
    static HistList getRedoList() {
        return redolist;
    }
    static void setRedoListNull() {
        redolist = null;
    }
    public void redo() {
        run();
    }
    public void run() {
        mybuttonrc.swap();
        history.push(this);
    }
    public void undo(){
        mybuttonrc.backswap();
        if(redolist==null) {
            redolist = new HistList();
        }
        redolist.push(this);
    }
}
