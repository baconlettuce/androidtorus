package com.example.t1330.androidtorus;

import android.content.Context;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by t1330 on 2016/03/09.
 */
public class MyTableLayout extends TableLayout implements View.OnClickListener{
    Context context;

    private TableRow current_tablerow;
    private int current_tablerow_size=0;
    MyButtons buttons;
    final int buttonsize = 150;
    int rc_size;

    MyTableLayout(Context context) {
        super(context);
        this.context = context;
    }
    
    void tablereset(int rc_size) {
        removeAllViews();

        this.rc_size = rc_size;

        buttons = new MyButtons(context, rc_size,rc_size, buttonsize);

        // historyの初期化・設定
        RCButton.init(this, new HistList());

        //colswitchbuttonsの初期化
        //rowswitchbuttonsの初期化
        init_button();
        buttons.shuffle();
    }
    void init_button() {
        addButton(new View(context));
        for (int i=0; i<rc_size; i++){
            addButton(buttons.getMyButtoncol(i).makeRCButton(context));
        }
        for (int i=0; i<rc_size; i++) {
            addButton(buttons.getMyButtonrow(i).makeRCButton(context));
            for(int j=0; j<rc_size; j++){
                addButton(buttons.getMyButtonrow(i).getMyButton((j)));
            }
        }
    }
    public void setCompleteView() {
        TextView textview = new TextView(context);
        textview.setText("Completed!!");
        textview.setTextSize(buttonsize*rc_size*2/textview.getText().length());
        addView(textview);
    }

    private void addButton(View view) {
        if (current_tablerow==null || current_tablerow_size>rc_size) {
            addTableRow();
        }
        current_tablerow.addView(view, buttonsize, buttonsize);
        current_tablerow_size++;
    }
    private void addTableRow() {
        current_tablerow = new TableRow(context);
        current_tablerow_size = 0;
        addView(current_tablerow);
    }
    @Override
    public void onClick(View v) {
        RCButton button = (RCButton)v;
        button.run();
        RCButton.setRedoListNull();
        if(judge()) {
            setCompleteView();
        }
    }
    void addImage() {
        buttons.switchisdisplayimage();
    }
    void undo() {
        HistLink link;
        if ((link=RCButton.getHistory().pop()) != null) {
            link.getButton().undo();
        }
    }
    void redo() {
        HistList redolist;
        HistLink link;
        if ((redolist=RCButton.getRedoList())!=null && (link=redolist.pop()) != null) {
            link.getButton().redo();
        }
    }
    boolean judge() {
        return buttons.judge();
    }
}
