package com.example.t1330.androidtorus;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;

public class MainActivity extends AppCompatActivity {
    TableLayout basetable;
    MyTableLayout mytablelayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Display display = getWindowManager().getDefaultDisplay();
        Point window_size = new Point();
        display.getSize(window_size);

        basetable = (TableLayout) findViewById(R.id.lebeltable);


        // levelボタンの初期設定
        TableRow levelrow = new TableRow(this);
        init_levelrow(levelrow);

        //toolrowの初期化設定
        TableRow toolrow = new TableRow(this);
        init_toolrow(toolrow);

        basetable.addView(levelrow);
        basetable.addView(toolrow);
        mytablelayout = new MyTableLayout(this);
        basetable.addView(mytablelayout);
    }
    void init_levelrow(TableRow levelrow) {
        for(int i=2; i<6; i++) {
            Button button = new LevelButton(this,i);
            levelrow.addView(button,200,200);
        }
    }
    void init_toolrow(TableRow toolrow) {
        ToolButton toolbutton = new ToolButton(this, "戻る");
        toolbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToolButton tb = (ToolButton) v;
                tb.undo();
            }
        });
        toolrow.addView(toolbutton,200,200);

        toolbutton = new ToolButton(this, "進む");
        toolbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToolButton tb = (ToolButton) v;
                tb.redo();
            }
        });
        toolrow.addView(toolbutton,200,200);

        toolbutton = new ToolButton(this, "画像");
        toolbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToolButton tb = (ToolButton) v;
                tb.addImage();
            }
        });
        toolrow.addView(toolbutton, 200, 200);
    }
    private class LevelButton extends Button{
        int level;
        Context context;

        LevelButton(Context context, int level) {
            super(context);

            this.context = context;
            this.level= level;
            setText("LEVEL\n"+level);

            setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createMyTableLayout();
                }
            });
        }
        void createMyTableLayout() {
            mytablelayout.tablereset(level);
        }
    }
    private class ToolButton extends Button {

        ToolButton(Context context, String str) {
            super(context);

            setText(str);
        }

        void undo() {
            if (mytablelayout != null) {
                mytablelayout.undo();
            }
        }

        void redo() {
            if (mytablelayout !=null) {
                mytablelayout.redo();
            }
        }
        void addImage() {
            if (mytablelayout !=null) {
                mytablelayout.addImage();
            }
        }
    }
}
